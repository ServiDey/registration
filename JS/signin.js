$("#signin").on("click", function(){
    let login = $("#login").val().trim();
    let password = $("#password").val().trim();

    if(login.length < 6 || password.length < 6) {
      return false;
    }

    $.ajax({
        url: 'ajax/signin.php',
        type: 'POST',
        cache: false,
        data: {
          'login': login,
          'password': password,
        },
        dataType: 'html',
        beforeSend: function() {
          $("#signin").prop("disabled", true);
        },
        success: function(data) {
          $("#signin").prop("disabled", false);
          document.location.replace("profile.php");
        }
    });
});