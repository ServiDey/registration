$("#button").on("click", function(){
    let login = $("#login").val().trim();
    let password = $("#password").val().trim();
    let confirm_password = $("#confirm_password").val().trim();
    let email = $("#email").val().trim();
    let name = $("#name").val().trim();

    let arrayPassword = password.split('');
    let lowercase = "qwertyuiopasdfghjklzxcvbnm";
    let uppercase = "QWERTYUIOPASDFGHJKLZXCVBNM";
    let digits = "0123456789";
    let specials = "!@#$%^&*()_+=-?.,<>{}[]`~;:|/";

    let l = 0;
    function lowercaseIndex(l){
    for (let elem of arrayPassword){
      for(let i = 0; i<lowercase.length; i++){
          if(elem == lowercase[i]){
            l = 1;
      }
    }
    }
    return l;
    }
    let indexLowercase = lowercaseIndex(l);

    let u = 0;
    function uppercaseIndex(u){
    for (let elem of arrayPassword){
      for(let i = 0; i<uppercase.length; i++){
          if(elem == uppercase[i]){
            u = 1;
      }
    }
    }
    return u;
    }
    let indexUppercase = uppercaseIndex(u);

    let d = 0;
    function digitsIndex(d){
    for (let elem of arrayPassword){
      for(let i = 0; i<digits.length; i++){
          if(elem == digits[i]){
            d = 1;
      }
    }
    }
    return d;
    }
    let indexDigits = digitsIndex(d);

    let s = 0;
    function specialsIndex(s){
    for (let elem of arrayPassword){
      for(let i = 0; i<specials.length; i++){
          if(elem == specials[i]){
            s = 1;
      }
    }
    }
    return s;
    }
    let indexSpecials = specialsIndex(s);

    console.log()

    if(login.length < 6) {
      $("#errorLogin").css('display', 'block');
      return false;
    } else if (password.length < 6) {
      $("#errorPassword").css('display', 'block');
      return false;
    } else if (indexLowercase == 0) {
      $("#easyPassword").css('display', 'block');
      return false;
    } else if (indexUppercase == 0) {
      $("#easyPassword").css('display', 'block');
      return false;
    } else if (indexDigits == 0) {
      $("#easyPassword").css('display', 'block');
      return false;
    } else if (indexSpecials == 0) {
      $("#easyPassword").css('display', 'block');
      return false;
    } else if (password != confirm_password) {
      $("#error").css('display', 'block');
      return false;
    } else if (name.length != 2) {
      $("#errorName").css('display', 'block');
      return false;
    }

    $.ajax({
        url: 'ajax/signup.php',
        type: 'POST',
        cache: false,
        data: {
          'login': login,
          'password': password,
          'confirm_password': confirm_password,
          'email': email,
          'name': name,
        },
        dataType: 'html',
        beforeSend: function() {
          $("#button").prop("disabled", false);
        },
        success: function(data) {
          alert('Регистрирация прошла успешно');
          document.location.replace("authorization.php");
        }
    });
});