<?php
    session_start();
    if(isset($_SESSION['name'])){
       header("Location: ../profile.php");
     }
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Авторизация</title>
	    <link rel='stylesheet' href="css/style.css">
	</head>
	<body>
        <form id='form'>
          <label>Login</label>
          <input id="login" type="text" name="login" placeholder="Введите логин" value="<?php if (isset($_SESSION["login"])) echo $_SESSION['login']; ?>">
          <label>Password</label>
          <input id="password" type="Password" name="password" placeholder="Введите пароль" value="<?php if (isset($_SESSION['password'])) echo $_SESSION['password'];  ?>">
          <input type="button" name="signin" id="signin" value="Войти">
          <p><a href="index.php" <?php ?> >Регистрация</a> при отсутствии аккаунта</p>
        </form>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src='JS/signin.js'></script>
</html>