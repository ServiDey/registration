<?php
    session_start();
    if(isset($_SESSION['name'])){
       header("Location: ../profile.php");
     }
    unset($_SESSION['login']);
    unset($_SESSION['password']);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Регистрация</title>
	    <link rel='stylesheet' href="css/style.css">
	</head>
	<body>
    <div>
        <form id='form'>
          <label>Login (unique)</label>
          <input type="text" id="login" name="login" placeholder="Введите логин" required>
          <p id="errorLogin">Логин должен содержать минимум 6 символов</p>
          <label>Password</label>
          <input type="Password" id="password" name="password" placeholder="Введите пароль" required>
          <p id="errorPassword">Пароль должен содержать минимум 6 символов</p>
          <p id="easyPassword">Слишком простой пароль</p>
          <label>Confirm password</label>
          <input type="Password" id="confirm_password" name="confirm_password" placeholder="Повторите пароль" required>
          <p id="error">Пароли не совпали</p>
          <label>Email</label>
          <input type="Email" id="email" name="email" placeholder="Введите адрес электронной почты" required>
          <label>Name</label>
          <input type="text" id="name" name="name" placeholder="Введите имя учетной записи" required>
          <p id="errorName">Имя должно содержать 2 символа</p>
          <input type="button" id='button' value='Регистрирация'>
          <p>Я уже зарегитрировался - <a href="authorization.php">Войти</a></p>
        </form>
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src='JS/signup.js'></script>
</html>