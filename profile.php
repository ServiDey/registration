<?php
    session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Страница пользователя</title>
	    <link rel='stylesheet' href="css/style.css">
	</head>
	<body>
        <div>
          <h1>Hello <?= $_SESSION['name'] ?></h1>
          <a href='logout.php'>Выйти</a>
        </div>
  </body>
</html>